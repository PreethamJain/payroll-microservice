@Library('jenkins-library@0.27.0') _

gradleVersion='4.10.3'
gradle="/opt/gradle/gradle-${gradleVersion}/bin/gradle"
appName = 'new-project-template-v5'

pipeline {
  agent any
  options {
    disableConcurrentBuilds()
    skipDefaultCheckout true
  }

  environment {
    webhookUrl = credentials('ph-build-status')
  }

  stages {
    stage('Clean WS and checkout SCM') {
      steps {
        deleteDir()
        checkout scm
      }
    }
    stage('Clean') {
      steps {
        script {
          sh "${gradle} clean"
        }
      }
    }

    stage('Increment Version') {
      steps {
        script {
          if (versions.isReleaseBuild()) {
            versions.increment('java')
          }
        }
      }
    }

    stage('Build and publish artifacts') {
      steps {
        script {
          version = versions.getVersion('java', false)
          sh "${gradle} build -PappVersion=${version} -Prunenv=dev"
          artifacts.publishPeoplehum("build/libs/${appName}-${version}.jar", "${appName}/${version}")
          artifacts.publishPeoplehum("config/logback-spring.xml", "${appName}/${version}/config")
          artifacts.publishPeoplehum("config/logback-access-spring.xml", "${appName}/${version}/config")
        }
      }
    }

    stage ('Push to Git and Update Deployment Repo') {
      steps {
        script {
          if (versions.isReleaseBuild()) {
            sshagent(['jenkins-coviam']) {
              sh "git push origin HEAD:$BRANCH_NAME --tags"
            }
            manifest.publish(
              'new-project-template-v5', // i hate the underscores, but ansible needs it
              'git@gitlab.com:peoplehum/deployment-ansible.git',
              'envs/qa',
              versions.getVersion('java')
            )
          }
        }
      }
    }
  }
  post {
    always {
      script {
        notification.buildStatusTeams(webhookUrl)
      }
      cleanWs()
    }
  }
}
