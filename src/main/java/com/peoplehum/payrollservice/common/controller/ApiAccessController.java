package com.peoplehum.payrollservice.common.controller;

import com.nethum.custom.annotation.model.ApiMapping;
import com.peoplehum.payrollservice.common.service.ApiAccessService;
import com.peoplehum.payrollservice.common.service.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by peoplehum on 23/06/20.
 */
@RestController
@CrossOrigin
@RequestMapping(value = "/v1.0")
public class ApiAccessController {

  @Autowired
  private TokenService tokenService;

  @Autowired
  @Qualifier("com.peoplehum.payrollservice.common.service.ApiAccessService")
  private ApiAccessService apiAccessService;

  @GetMapping(value = "/api/access")
  public ResponseEntity<List<ApiMapping>> getApiAccesses() {
    System.out.println("controller");
    List<ApiMapping> accesses = apiAccessService.getApiAccesses();
    return new ResponseEntity<>(accesses, HttpStatus.OK);
  }

//  @GetMapping(value = "/generateToken")
//  public String getAccessToken() throws UnsupportedEncodingException {
//    System.out.println("tokenController");
//    return tokenService.createJWT(companyDomain, companyHash, partnerId, secretKey);
//  }

}
