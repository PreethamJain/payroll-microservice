package com.peoplehum.payrollservice.common.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.peoplehum.payrollservice.common.dto.*;
import com.peoplehum.payrollservice.common.service.PayrollOperations;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.UnsupportedEncodingException;

@RestController
@CrossOrigin
public class PayrollController {

    @Autowired
    private PayrollOperations payrollOperations;

    //Add Single Employee
    @PutMapping(value = "/addSingleEmployee")
    public String addSingleEmployee(@RequestBody GenericRequestDto<EmployeeDto> genericRequestDto) throws UnsupportedEncodingException, JsonProcessingException {

        CommonVariablesDto commonVariablesDto = new CommonVariablesDto();
        BeanUtils.copyProperties(genericRequestDto, commonVariablesDto);
        commonVariablesDto.setClientName("peoplehum");
        return payrollOperations.addSingleEmployee(commonVariablesDto, genericRequestDto.getRequestBody());
    }

    //Add Bulk Employees
    @PutMapping(value = "/addBulkEmployees")
    public String addBulkEmployees(@RequestBody GenericRequestDto<BulkAddEmployeeDto> genericRequestDto) throws JsonProcessingException {
        CommonVariablesDto commonVariablesDto = new CommonVariablesDto();
        BeanUtils.copyProperties(genericRequestDto, commonVariablesDto);
        commonVariablesDto.setClientName("peoplehum");
        return payrollOperations.addBulkEmployees(commonVariablesDto, genericRequestDto.getRequestBody());
    }

    //Update Employee By Code
    @PutMapping(value = "/updateEmployee")
    public String updateEmployeeByCode(@RequestBody GenericRequestDto<UpdateEmployeeDto> genericRequestDto) {

        CommonVariablesDto commonVariablesDto = new CommonVariablesDto();
        BeanUtils.copyProperties(genericRequestDto, commonVariablesDto);
        commonVariablesDto.setClientName("peoplehum");
        return payrollOperations.updateEmployee(commonVariablesDto, genericRequestDto.getRequestBody());
    }

    //Employee Resignation
    @PutMapping(value = "/employeeResignation")
    public String employeeResignation(@RequestBody GenericRequestDto<EmployeeResignationDto> genericRequestDto) throws JsonProcessingException {

        CommonVariablesDto commonVariablesDto = new CommonVariablesDto();
        BeanUtils.copyProperties(genericRequestDto, commonVariablesDto);
        commonVariablesDto.setClientName("peoplehum");
        return payrollOperations.employeeResignation(commonVariablesDto, genericRequestDto.getRequestBody());
    }

    //Employee Salary Increment
    @PutMapping(value = "/employeeSalaryUpdate")
    public String employeeSalaryUpdate(@RequestBody GenericRequestDto<SalaryDto> genericRequestDto) throws JsonProcessingException {
        CommonVariablesDto commonVariablesDto = new CommonVariablesDto();
        BeanUtils.copyProperties(genericRequestDto, commonVariablesDto);
        commonVariablesDto.setClientName("peoplehum");
        return payrollOperations.employeeSalaryUpdate(commonVariablesDto, genericRequestDto.getRequestBody());
    }

}
