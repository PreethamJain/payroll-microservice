package com.peoplehum.payrollservice.common.dto;

import lombok.Data;

import java.util.List;

@Data
public class BulkAddEmployeeDto {

    private List<EmployeeDto> employees;
}
