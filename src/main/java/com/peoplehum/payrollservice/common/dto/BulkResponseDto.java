package com.peoplehum.payrollservice.common.dto;

import lombok.Data;

import java.util.List;

@Data
public class BulkResponseDto {

    private Long status;
    private String message;
    private List<PutResponseDto> responseStatus;
}
