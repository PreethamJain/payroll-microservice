package com.peoplehum.payrollservice.common.dto;

import lombok.Data;

@Data
public class CommonVariablesDto {
    private Long customerId;
    private Long payrollProviderId;
    private String clientName;
    private Long clientId;
}
