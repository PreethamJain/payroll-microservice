package com.peoplehum.payrollservice.common.dto;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EmployeeDto {

    private String userId;

    private String code; //Mandatory
    private String name; //Mandatory
    private String gender; //Mandatory
    private String emailId;
    private String birthDate; //Mandatory
    private String dateOfJoining; //Mandatory
    private double annualCTC;
    private String employeePAN;
    private String fatherName;
    private String address;
    private String otherEmail;
    private String mobile;
    private String maritalStatus;
    private String workLocation; //Mandatory {​​​​​​​​Should be one of those specified by Client at Paybooks}​​​​​​​​
    private String city;
    private String state;
    private String pincode;
    private String emergencyContactNumber;
    private String landlinePhoneNumber;
    private String passportNumber;
    private String passportExpiryDate;
    private String bloodGroup;
    private String nameOfSpouse;
    private int confirmationperiod;
    private String level;
    private String department;
    private String designation;
    private String costCenter;
    private String jobType;
    private boolean isReportingManager;
    private boolean hasESSAccess;
    private String secondaryReportingManager;
    private String secondaryReportingManagerEmail;
    private double fbp;
    private double variablePay;
    private String salaryPaymentMode;
    private String companyBank;
    private String employeeBankAccountNumber;
    private String employeeIFSC;
    private String employeeBank;
    private String pfNumber;
    private String pfDate;
    private String uan;
    private boolean isPFRestricted;
    private boolean isEntitledtoEPS;
    private String esiNumber;
    private boolean enableNewJoineeArrears; //Mandatory {​​​​​​​​“True” enable for computing for new joiner arrear process.}​​​​​​​​







}
