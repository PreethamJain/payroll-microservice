package com.peoplehum.payrollservice.common.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeIncrementDetails {
    private String userId;
    private String code;
    private String effectiveDate;
    private Double incrementedCTC;
}
