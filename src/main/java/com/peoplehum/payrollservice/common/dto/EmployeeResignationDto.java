package com.peoplehum.payrollservice.common.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeResignationDto {

    private String userId;

    private String code;
    private String lastWorkingDate;
    private String resignationDate;
    private String reasonForLeaving;
    private String payProcessType;
    private Boolean isSettled;
    private String esiRemarks;
    private String remarks;
}
