package com.peoplehum.payrollservice.common.dto;

import lombok.Data;

@Data
public class GenericRequestDto<T> {
    private Long customerId;
    private Long payrollProviderId;
    private T requestBody;
}
