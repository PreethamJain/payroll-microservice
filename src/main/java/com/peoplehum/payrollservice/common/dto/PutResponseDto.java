package com.peoplehum.payrollservice.common.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PutResponseDto {
    private Long status;
    private String message;
}
