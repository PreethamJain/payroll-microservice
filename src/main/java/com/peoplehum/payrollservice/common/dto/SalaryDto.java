package com.peoplehum.payrollservice.common.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SalaryDto {
    private List<EmployeeIncrementDetails> employeeIncrementDetails;
}
