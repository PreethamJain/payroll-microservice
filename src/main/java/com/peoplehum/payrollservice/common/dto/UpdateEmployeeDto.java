package com.peoplehum.payrollservice.common.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UpdateEmployeeDto {

    private String userId;

    private String code;
    private String name;
    private String gender;
    private String emailId;
    private String birthDate;
    private String dateOfJoining;
    private String employeePAN;
    private String fatherName;
    private String address;
    private String otherEmail;
    private String mobile;
    private String maritalStatus;
    private String workLocation;
    private String department;
    private String designation;
    private String jobType;
    private String city;
    private String pincode;
    private String passportNumber;
    private String passportExpiryDate;
    private String employeeBankAccountNumber;
    private String employeeIFSC;
    private String employeeBank;
    private String pfNumber;
    private String pfDate;
    private String uan;
    private String esiNumber;
}
