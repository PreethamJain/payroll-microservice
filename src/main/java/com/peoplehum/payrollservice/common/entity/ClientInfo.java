package com.peoplehum.payrollservice.common.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Data
@Entity
public class ClientInfo {
    @Id
    @GenericGenerator(name = "client_id_seq", strategy = "increment")
    @GeneratedValue(generator = "client_id_seq", strategy = GenerationType.AUTO)
    private Long clientId;

    @Column(unique = true)
    private String clientName;

    @OneToOne(mappedBy = "clientInfo", cascade = CascadeType.ALL)
//    @JoinColumn(name = "client_secret_details_id", referencedColumnName = "clientId")
    @PrimaryKeyJoinColumn
    private ClientSecretDetails clientSecretDetails;


}
