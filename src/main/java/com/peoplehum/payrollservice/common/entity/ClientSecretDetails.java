package com.peoplehum.payrollservice.common.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class ClientSecretDetails {

    @Id
    private Long clientId;

    private String clientKey;

    private String clientSecret;

    @OneToOne
    @MapsId
    @JoinColumn(name = "client_id")
    private ClientInfo clientInfo;
}
