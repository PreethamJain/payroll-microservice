package com.peoplehum.payrollservice.common.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

@Data
@Entity
@IdClass(PaybooksCompanyInfoId.class)
public class PaybooksCompanyInfo {
    @Id
    private Long customerId;

    @Id
    private Long clientId;

    private String companyHash;

    private String companyDomain;

    @Column(length = 512)
    private String accessToken;
}
