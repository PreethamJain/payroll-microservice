package com.peoplehum.payrollservice.common.entity;

import java.io.Serializable;

public class PaybooksCompanyInfoId implements Serializable {

    private Long customerId;

    private Long clientId;

    public PaybooksCompanyInfoId() {}

    public PaybooksCompanyInfoId(Long customerId, Long clientId) {
        this.customerId = customerId;
        this.clientId = clientId;
    }
}
