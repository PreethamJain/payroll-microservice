package com.peoplehum.payrollservice.common.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

@Data
@Entity
@IdClass(PayrollUserInfoId.class)
public class PayrollUserInfo {
    @Id
    private String userId;

    @Id
    private Long clientId;

    private Long customerId;
    private String externalUserId;

}
