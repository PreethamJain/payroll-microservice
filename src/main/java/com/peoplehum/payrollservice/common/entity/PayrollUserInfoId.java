package com.peoplehum.payrollservice.common.entity;

import java.io.Serializable;

public class PayrollUserInfoId implements Serializable {
    private String userId;

    private Long clientId;

    public PayrollUserInfoId() {}

    public PayrollUserInfoId(String userId, Long clientId) {
        this.userId = userId;
        this.clientId = clientId;
    }
}
