package com.peoplehum.payrollservice.common.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Entity
public class PayrollVendorMapping {
    @Id
    @GenericGenerator(name = "payroll_provider_id_seq", strategy = "increment")
    @GeneratedValue(generator = "payroll_provider_id_seq", strategy = GenerationType.AUTO)
    private Long payrollProviderId;

    private String payrollVendorName;
}
