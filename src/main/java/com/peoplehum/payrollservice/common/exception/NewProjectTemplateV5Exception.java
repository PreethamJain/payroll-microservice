package com.peoplehum.payrollservice.common.exception;

import com.nethum.errorhandling.exception.NethumBaseException;
import com.nethum.errorhandling.exception.error.AppErrorObject;
import com.peoplehum.payrollservice.common.model.NewProjectTemplateV5StatusCode;
import org.springframework.http.HttpStatus;

/**
 * Created by peoplehum on 23/06/20.
 */
public class NewProjectTemplateV5Exception extends NethumBaseException {

  private static final long serialVersionUID = 6696549368956277964L;

  /**
   * @param statusCode
   */
  public NewProjectTemplateV5Exception(NewProjectTemplateV5StatusCode statusCode) {
    super(new AppErrorObject.Builder().appCode(statusCode).HttpStatus(HttpStatus.OK).build());
  }

  /**
   * @param statusCode
   * @param httpStatus
   */
  public NewProjectTemplateV5Exception(NewProjectTemplateV5StatusCode statusCode, HttpStatus httpStatus) {
    super(new AppErrorObject.Builder().appCode(statusCode).HttpStatus(httpStatus).build());
  }

  public NewProjectTemplateV5Exception(NewProjectTemplateV5StatusCode code, Throwable throwable) {
    super(new AppErrorObject.Builder().throwable(throwable).HttpStatus(HttpStatus.OK).appCode(code)
        .build());
  }
}
