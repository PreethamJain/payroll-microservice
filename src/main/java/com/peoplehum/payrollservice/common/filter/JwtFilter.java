package com.peoplehum.payrollservice.common.filter;

import com.peoplehum.payrollservice.common.entity.ClientInfo;
import com.peoplehum.payrollservice.common.service.payrollServiceImpl.ClientInfoService;
import com.peoplehum.payrollservice.common.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;


@Component("com.peoplehum.payrollservice.common.filter.JwtFilter")
public class JwtFilter extends OncePerRequestFilter {

    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private ClientInfoService clientInfoService;


    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        String authorizationHeader = request.getHeader("Authorization");
        String token = null;
        String clientName = null;

        if(authorizationHeader!=null && authorizationHeader.startsWith("Token")){
            token=authorizationHeader.substring(6);
            clientName=jwtUtil.extractUsername(token);
        }

        if(clientName!=null && SecurityContextHolder.getContext().getAuthentication()==null){
            ClientInfo clientInfo= clientInfoService.loadByClientName(clientName);
            if(jwtUtil.validateToken(token,clientInfo)){
                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(clientInfo, null, new ArrayList<>());
                authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
        }

        filterChain.doFilter(request,response);
    }
}

