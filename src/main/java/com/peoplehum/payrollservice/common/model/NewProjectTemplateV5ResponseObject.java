package com.peoplehum.payrollservice.common.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;

/**
 * Created by peoplehum on 23/06/20.
 */
@Slf4j
@ToString(doNotUseGetters = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class NewProjectTemplateV5ResponseObject<T> {
  private T responseObject;
  private NewProjectTemplateV5StatusCode status;

  @JsonIgnore
  private HttpStatus statusCode;

  public NewProjectTemplateV5ResponseObject() {
    this.statusCode = HttpStatus.OK;
  }

  public NewProjectTemplateV5ResponseObject(NewProjectTemplateV5StatusCode status) {
    this();
    this.status = status;
  }

  public NewProjectTemplateV5ResponseObject(NewProjectTemplateV5StatusCode status, T responseObject) {
    this(status);
    this.responseObject = responseObject;
  }

}
