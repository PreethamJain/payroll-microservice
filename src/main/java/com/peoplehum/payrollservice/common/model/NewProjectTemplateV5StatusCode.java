package com.peoplehum.payrollservice.common.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.nethum.errorhandling.exception.error.AppCode;
import com.nethum.errorhandling.exception.error.CodeDesc;

import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by peoplehum on 23/06/20.
 */
public enum NewProjectTemplateV5StatusCode implements AppCode<NewProjectTemplateV5StatusCode> {

  SUCCESS(1000, "SUCCESS"),
  PROCESSING_ERROR(999, "PROCESSING_ERROR");


  private static Map<Integer, NewProjectTemplateV5StatusCode> formatMap =
      Stream.of(NewProjectTemplateV5StatusCode.values())
          .collect(Collectors.toMap(s -> s.code, Function.identity()));

  private final int code;

  private final String desc;

  NewProjectTemplateV5StatusCode(int code, String desc) {
    this.code = code;
    this.desc = desc;
  }

  @JsonCreator // This is the factory method and must be static
  public static NewProjectTemplateV5StatusCode fromJson(CodeDesc codeDesc) {
    return Optional.ofNullable(formatMap.get(codeDesc.getCode()))
        .orElseThrow(() -> new IllegalArgumentException(codeDesc.toString()));
  }

  // validity of status code
  public static Boolean validType(String type) {
    if (formatMap.containsKey(type)) {
      return Boolean.TRUE;
    }
    return Boolean.FALSE;
  }

  /**
   * Return the integer code of this status code.
   */
  public int getCode() {
    return this.code;
  }

  /**
   * Return the reason phrase of this status code.
   */
  public String getDesc() {
    return desc;
  }

  /**
   * Return a string representation of this status code.
   */
  @Override
  public String toString() {
    return Integer.toString(code);
  }

  /**
   * @param statusCode
   *
   * @return
   */
  @Override
  public NewProjectTemplateV5StatusCode valueOf(int statusCode) {
    for (NewProjectTemplateV5StatusCode status : values()) {
      if (status.code == statusCode) {
        return status;
      }
    }
    throw new IllegalArgumentException("No matching constant for [" + statusCode + "]");
  }
}
