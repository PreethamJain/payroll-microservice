package com.peoplehum.payrollservice.common.repository;

import com.peoplehum.payrollservice.common.entity.ClientInfo;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientInfoRepository extends CrudRepository<ClientInfo, Long> {

    @Query(value = "SELECT clientId FROM clientInfo WHERE clientName=?1", nativeQuery = true)
    Long getClientIdByName(String clientName);

    @Query(value = "SELECT * FROM clientInfo WHERE clientInfo.clientName= ?1", nativeQuery = true)
    ClientInfo loadByClientName(String username);
}
