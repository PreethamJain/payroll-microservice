package com.peoplehum.payrollservice.common.repository;

import com.peoplehum.payrollservice.common.entity.PaybooksCompanyInfo;
import com.peoplehum.payrollservice.common.entity.PaybooksCompanyInfoId;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PaybooksCompanyInfoRepository extends CrudRepository<PaybooksCompanyInfo, PaybooksCompanyInfoId> {

}
