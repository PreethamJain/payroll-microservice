package com.peoplehum.payrollservice.common.repository;

import com.peoplehum.payrollservice.common.entity.PayrollUserInfo;
import com.peoplehum.payrollservice.common.entity.PayrollUserInfoId;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PayrollUserInfoRepository extends CrudRepository<PayrollUserInfo, PayrollUserInfoId> {

    @Query(value = "SELECT externalUserId FROM payrollUserInfo WHERE userId=?1 AND clientId=?2", nativeQuery = true)
    String getExternalUserId(String userId, Long clientId);
}
