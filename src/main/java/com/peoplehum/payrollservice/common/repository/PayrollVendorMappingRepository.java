package com.peoplehum.payrollservice.common.repository;

import com.peoplehum.payrollservice.common.entity.PayrollVendorMapping;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PayrollVendorMappingRepository extends CrudRepository<PayrollVendorMapping, Long> {
}
