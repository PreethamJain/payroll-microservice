package com.peoplehum.payrollservice.common.service;

import com.nethum.custom.annotation.model.ApiMapping;
import com.nethum.custom.annotation.operation.Action;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * Created by peoplehum on 23/06/20.
 */
@Slf4j
@Service("com.peoplehum.payrollservice.common.service.ApiAccessService")
public class ApiAccessService {

  @Autowired
  ListableBeanFactory listableBeanFactory;

  private Action action;

  @PostConstruct
  public void setup() {
    action = new Action();
  }

  public List<ApiMapping> getApiAccesses() {
    System.out.println("here");
    log.info("getting accesses and entitlements for API's of one2one ListableBeanFactory : {}",
        listableBeanFactory);
    return action.getApiAccess(listableBeanFactory);
  }
}
