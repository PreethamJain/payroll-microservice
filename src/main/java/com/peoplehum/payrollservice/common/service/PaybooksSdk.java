package com.peoplehum.payrollservice.common.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.peoplehum.payrollservice.common.dto.*;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import org.springframework.web.util.UriComponentsBuilder;

@Service
public class PaybooksSdk {

    WebClient webClient=WebClient.builder().baseUrl("https://mailapi.paybooks.in/v1/company").build();

    public PutResponseDto callAddSingleEmployee(String accessToken, String companyDomain,EmployeeDto employeeDto) throws JsonProcessingException {
        PutResponseDto responseDto;

        try {
            responseDto = webClient.put()
                    .uri("/" + companyDomain + "/employee")
                    .header("Authorization","token"+" "+accessToken)
                    .syncBody(employeeDto)
                    .retrieve()
                    .bodyToMono(PutResponseDto.class)
                    .block();
        } catch (WebClientResponseException.BadRequest badRequest) {
            ObjectMapper objectMapper = new ObjectMapper();
            responseDto = objectMapper.readValue(badRequest.getResponseBodyAsString(), PutResponseDto.class);
            return responseDto;
        } catch (WebClientResponseException.Unauthorized unauthorized) {
            ObjectMapper objectMapper = new ObjectMapper();
            responseDto = objectMapper.readValue(unauthorized.getResponseBodyAsString(), PutResponseDto.class);
            return responseDto;
        }
        return responseDto;
    }

    public BulkResponseDto callAddBulkEmployees(String accessToken, String companyDomain, BulkAddEmployeeDto bulkAddEmployeeDto) throws JsonProcessingException {
        BulkResponseDto responseDto;

        try {
            responseDto = webClient.put()
                    .uri("/" + companyDomain + "/employees")
                    .header("Authorization","token"+" "+accessToken)
                    .syncBody(bulkAddEmployeeDto)
                    .retrieve()
                    .bodyToMono(BulkResponseDto.class)
                    .block();
        } catch (WebClientResponseException.BadRequest badRequest) {
            ObjectMapper objectMapper = new ObjectMapper();
            responseDto = objectMapper.readValue(badRequest.getResponseBodyAsString(), BulkResponseDto.class);
            return responseDto;
        } catch (WebClientResponseException.Unauthorized unauthorized) {
            ObjectMapper objectMapper = new ObjectMapper();
            responseDto = objectMapper.readValue(unauthorized.getResponseBodyAsString(), BulkResponseDto.class);
            return responseDto;
        }
        return responseDto;
    }

    public PutResponseDto callUpdateEmployeeByCode(String accessToken, String companyDomain, UpdateEmployeeDto updateEmployeeDto) throws JsonProcessingException {

        String uri= UriComponentsBuilder.fromUriString("/" + companyDomain + "/employee")
                .queryParam("operation","update")
                .build()
                .toUriString();

        PutResponseDto responseDto;

        try {
            responseDto = webClient.post()
                    .uri(uri)
                    .header("Authorization","token"+" "+accessToken)
                    .syncBody(updateEmployeeDto)
                    .retrieve()
                    .bodyToMono(PutResponseDto.class)
                    .block();
        } catch (WebClientResponseException.BadRequest badRequest) {
            ObjectMapper objectMapper = new ObjectMapper();
            responseDto = objectMapper.readValue(badRequest.getResponseBodyAsString(), PutResponseDto.class);
            return responseDto;
        } catch (WebClientResponseException.Unauthorized unauthorized) {
            ObjectMapper objectMapper = new ObjectMapper();
            responseDto = objectMapper.readValue(unauthorized.getResponseBodyAsString(), PutResponseDto.class);
            return responseDto;
        }
        return responseDto;
    }

    public PutResponseDto callEmployeeResignation(String accessToken, String companyDomain, EmployeeResignationDto employeeResignationDto) throws JsonProcessingException {
        String uri= UriComponentsBuilder.fromUriString("/" + companyDomain + "/employee")
                .queryParam("operation","exit")
                .build()
                .toUriString();

        PutResponseDto responseDto;

        try {
            responseDto = webClient.post()
                    .uri(uri)
                    .header("Authorization","token"+" "+accessToken)
                    .syncBody(employeeResignationDto)
                    .retrieve()
                    .bodyToMono(PutResponseDto.class)
                    .block();
        } catch (WebClientResponseException.BadRequest badRequest) {
            ObjectMapper objectMapper = new ObjectMapper();
            responseDto = objectMapper.readValue(badRequest.getResponseBodyAsString(), PutResponseDto.class);
            return responseDto;
        } catch (WebClientResponseException.Unauthorized unauthorized) {
            ObjectMapper objectMapper = new ObjectMapper();
            responseDto = objectMapper.readValue(unauthorized.getResponseBodyAsString(), PutResponseDto.class);
            return responseDto;
        }
        return responseDto;
    }

    public BulkResponseDto callEmployeeSalaryUpdate(String accessToken, String companyDomain, SalaryDto salaryDto) throws JsonProcessingException {
        BulkResponseDto responseDto;
        try {
            responseDto = webClient.post()
                    .uri("/" + companyDomain + "/employees/increment")
                    .header("Authorization","token"+" "+accessToken)
                    .syncBody(salaryDto)
                    .retrieve()
                    .bodyToMono(BulkResponseDto.class)
                    .block();
        } catch (WebClientResponseException.BadRequest badRequest) {
            ObjectMapper objectMapper = new ObjectMapper();
            responseDto = objectMapper.readValue(badRequest.getResponseBodyAsString(), BulkResponseDto.class);
            return responseDto;
        } catch (WebClientResponseException.Unauthorized unauthorized) {
            ObjectMapper objectMapper = new ObjectMapper();
            responseDto = objectMapper.readValue(unauthorized.getResponseBodyAsString(), BulkResponseDto.class);
            return responseDto;
        }
        return responseDto;
    }
}
