package com.peoplehum.payrollservice.common.service;

import com.peoplehum.payrollservice.common.constants.PayrollVendorNameConstants;
import com.peoplehum.payrollservice.common.service.payrollServiceImpl.PaybooksService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PayrollFactory {

    @Autowired
    private PaybooksService paybooksService;

    public PayrollService getPayrollSoftware(String vendorName) {
        try{
            if (vendorName.equalsIgnoreCase(PayrollVendorNameConstants.PAYBOOKS_VENDOR_NAME)) {
                return paybooksService;
            }
        }catch (NullPointerException e) {
            System.out.println("no value present, nullpointer");
            return null;
        }

        return null;
    }
}
