package com.peoplehum.payrollservice.common.service;

import com.peoplehum.payrollservice.common.entity.PayrollVendorMapping;
import com.peoplehum.payrollservice.common.repository.PayrollVendorMappingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PayrollFactoryService {


    @Autowired
    private PayrollVendorMappingRepository payrollVendorMappingRepository;

    @Autowired
    private PayrollFactory payrollFactory;

    public PayrollService callFactoryService(Long payrollProviderId) {
        Optional<PayrollVendorMapping> payrollVendorMapping = Optional.of(new PayrollVendorMapping());
        try{
            payrollVendorMapping = payrollVendorMappingRepository.findById(payrollProviderId);

            return payrollFactory.getPayrollSoftware(payrollVendorMapping.get().getPayrollVendorName());

        }catch (NullPointerException e) {
            return null;
        }catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
