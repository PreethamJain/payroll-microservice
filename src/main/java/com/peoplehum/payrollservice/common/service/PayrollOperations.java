package com.peoplehum.payrollservice.common.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.peoplehum.payrollservice.common.dto.*;
import com.peoplehum.payrollservice.common.repository.ClientInfoRepository;
import com.peoplehum.payrollservice.common.repository.PayrollUserInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;

@Service
public class PayrollOperations {

    @Autowired
    private PayrollService payrollService;

    @Autowired
    private PayrollFactoryService payrollFactoryService;

    @Autowired
    private PayrollUserInfoRepository payrollUserInfoRepository;

    @Autowired
    private ClientInfoRepository clientInfoRepository;

    public String addSingleEmployee(CommonVariablesDto commonVariablesDto, EmployeeDto employeeDto) throws UnsupportedEncodingException, JsonProcessingException {
        try {
            commonVariablesDto.setClientId(clientInfoRepository.getClientIdByName(commonVariablesDto.getClientName()));
//        employeeDto.setCode(payrollUserInfoRepository.getExternalUserId(employeeDto.getUserId(), commonVariablesDto.getClientId()));
            employeeDto.setCode(employeeDto.getUserId());

            payrollService = payrollFactoryService.callFactoryService(commonVariablesDto.getPayrollProviderId());

            PutResponseDto responseDto = payrollService.addSingleEmployee(commonVariablesDto, employeeDto);

            return responseDto.getMessage();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    public String addBulkEmployees(CommonVariablesDto commonVariablesDto, BulkAddEmployeeDto bulkAddEmployeeDto) throws JsonProcessingException {
        try {
            commonVariablesDto.setClientId(clientInfoRepository.getClientIdByName(commonVariablesDto.getClientName()));

            for (EmployeeDto employee: bulkAddEmployeeDto.getEmployees()) {
                employee.setCode(employee.getUserId());
            }

            payrollService = payrollFactoryService.callFactoryService(commonVariablesDto.getPayrollProviderId());

            BulkResponseDto responseDto = payrollService.addBulkEmployees(commonVariablesDto, bulkAddEmployeeDto);

            return responseDto.getMessage();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String updateEmployee(CommonVariablesDto commonVariablesDto, UpdateEmployeeDto updateEmployeeDto) {
        try {
            commonVariablesDto.setClientId(clientInfoRepository.getClientIdByName(commonVariablesDto.getClientName()));

            updateEmployeeDto.setCode(updateEmployeeDto.getUserId());

            payrollService = payrollFactoryService.callFactoryService(commonVariablesDto.getPayrollProviderId());

            PutResponseDto responseDto = payrollService.updateEmployeeByCode(commonVariablesDto, updateEmployeeDto);

            return responseDto.getMessage();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String employeeResignation(CommonVariablesDto commonVariablesDto, EmployeeResignationDto resignationDto) throws JsonProcessingException {
        try {
            commonVariablesDto.setClientId(clientInfoRepository.getClientIdByName(commonVariablesDto.getClientName()));

            resignationDto.setCode(resignationDto.getUserId());

            payrollService = payrollFactoryService.callFactoryService(commonVariablesDto.getPayrollProviderId());

            PutResponseDto responseDto = payrollService.employeeResignation(commonVariablesDto, resignationDto);

            return responseDto.getMessage();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String employeeSalaryUpdate(CommonVariablesDto commonVariablesDto, SalaryDto salaryDto) throws JsonProcessingException {
        try {
            commonVariablesDto.setClientId(clientInfoRepository.getClientIdByName(commonVariablesDto.getClientName()));

            for (EmployeeIncrementDetails employee: salaryDto.getEmployeeIncrementDetails()) {
                employee.setCode(employee.getUserId());
            }

            payrollService = payrollFactoryService.callFactoryService(commonVariablesDto.getPayrollProviderId());

            BulkResponseDto responseDto = payrollService.employeeSalaryUpdate(commonVariablesDto, salaryDto);

            return responseDto.getMessage();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
