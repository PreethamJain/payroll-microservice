package com.peoplehum.payrollservice.common.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.peoplehum.payrollservice.common.dto.*;

import java.io.UnsupportedEncodingException;

public interface PayrollService {
//    public <T> PutResponseDto addSingleEmployee(String accessToken, T payrollRequestBody ) throws JsonProcessingException;


//    public BulkResponseDto addBulkEmployees(String accessToken, BulkAddEmployeeDto bulkAddEmployeeDto) throws JsonProcessingException;
//    public PutResponseDto updateEmployeeByCode(String accessToken, UpdateEmployeeDto updateEmployeeDto) throws JsonProcessingException;
//    public PutResponseDto employeeResignation(String accessToken, EmployeeResignationDto employeeResignationDto) throws JsonProcessingException;
//    public BulkResponseDto employeeSalaryUpdate(String accessToken, SalaryDto salaryDto) throws JsonProcessingException;

    public PutResponseDto addSingleEmployee(CommonVariablesDto commonVariablesDto, EmployeeDto requestDto) throws UnsupportedEncodingException, JsonProcessingException;
    public BulkResponseDto addBulkEmployees(CommonVariablesDto commonVariablesDto, BulkAddEmployeeDto bulkAddEmployeeDto) throws JsonProcessingException, UnsupportedEncodingException;
    public PutResponseDto updateEmployeeByCode(CommonVariablesDto commonVariablesDto, UpdateEmployeeDto updateEmployeeDto) throws JsonProcessingException, UnsupportedEncodingException;
    public PutResponseDto employeeResignation(CommonVariablesDto commonVariablesDto, EmployeeResignationDto employeeResignationDto) throws JsonProcessingException, UnsupportedEncodingException;
    public BulkResponseDto employeeSalaryUpdate(CommonVariablesDto commonVariablesDto, SalaryDto salaryDto) throws JsonProcessingException, UnsupportedEncodingException;

}
