package com.peoplehum.payrollservice.common.service.payrollServiceImpl;

import com.peoplehum.payrollservice.common.entity.ClientInfo;
import com.peoplehum.payrollservice.common.repository.ClientInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClientInfoService {

    @Autowired
    private ClientInfoRepository clientInfoRepository;


    public ClientInfo loadByClientName(String clientName){
     return  clientInfoRepository.loadByClientName(clientName);
    }
}
