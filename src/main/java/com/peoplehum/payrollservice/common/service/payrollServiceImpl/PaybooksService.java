package com.peoplehum.payrollservice.common.service.payrollServiceImpl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.peoplehum.payrollservice.common.dto.*;
import com.peoplehum.payrollservice.common.entity.PaybooksCompanyInfo;
import com.peoplehum.payrollservice.common.entity.PaybooksCompanyInfoId;
import com.peoplehum.payrollservice.common.entity.PayrollUserInfo;
import com.peoplehum.payrollservice.common.repository.PaybooksCompanyInfoRepository;
import com.peoplehum.payrollservice.common.repository.PayrollUserInfoRepository;
import com.peoplehum.payrollservice.common.service.PaybooksSdk;
import com.peoplehum.payrollservice.common.service.PayrollService;
import com.peoplehum.payrollservice.common.service.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.util.Optional;

@Service
public class PaybooksService implements PayrollService {

    @Autowired
    private PaybooksSdk paybooksSdk;

    @Autowired
    private PaybooksCompanyInfoRepository paybooksCompanyInfoRepository;

    @Autowired
    private PayrollUserInfoRepository payrollUserInfoRepository;

    @Autowired
    private TokenService tokenService;


    @Override
    public PutResponseDto addSingleEmployee(CommonVariablesDto commonVariablesDto, EmployeeDto employeeDto) throws UnsupportedEncodingException, JsonProcessingException {
        int refreshCount = 3;
        String accessToken = getToken(commonVariablesDto.getCustomerId(), commonVariablesDto.getClientId());
        String companyDomain = getCompanyDomain(commonVariablesDto.getCustomerId(), commonVariablesDto.getClientId());

        PutResponseDto responseDto = paybooksSdk.callAddSingleEmployee(accessToken, companyDomain, employeeDto);

        while(responseDto.getStatus() == 401 && responseDto.getMessage().equalsIgnoreCase("Unauthorised access") && refreshCount>0) {
                refreshCount--;
                accessToken = refreshToken(commonVariablesDto.getCustomerId(), commonVariablesDto.getClientId());
                responseDto = paybooksSdk.callAddSingleEmployee(accessToken, companyDomain, employeeDto);
            }

            if (refreshCount == 0) {
                responseDto.setStatus(403L);
                responseDto.setMessage("Unauthorised access! RefreshCount exceeded");

                return responseDto;
            }

            if (responseDto.getStatus() == 200 ) {
                PayrollUserInfo payrollUserInfo = new PayrollUserInfo();
                payrollUserInfo.setUserId(employeeDto.getUserId());
                payrollUserInfo.setCustomerId(commonVariablesDto.getCustomerId());
                payrollUserInfo.setClientId(commonVariablesDto.getClientId());
                payrollUserInfo.setExternalUserId(employeeDto.getCode());

                payrollUserInfoRepository.save(payrollUserInfo);
            }

        return responseDto;
    }

    @Override
    public BulkResponseDto addBulkEmployees(CommonVariablesDto commonVariablesDto, BulkAddEmployeeDto bulkAddEmployeeDto) throws JsonProcessingException, UnsupportedEncodingException {
        int refreshCount = 3;
        String accessToken = getToken(commonVariablesDto.getCustomerId(), commonVariablesDto.getClientId());
        String companyDomain = getCompanyDomain(commonVariablesDto.getCustomerId(), commonVariablesDto.getClientId());

        BulkResponseDto responseDto = paybooksSdk.callAddBulkEmployees(accessToken, companyDomain, bulkAddEmployeeDto);

        while(responseDto.getStatus() == 401 && responseDto.getMessage().equalsIgnoreCase("Unauthorised access") && refreshCount>0) {
                refreshCount--;
                accessToken = refreshToken(commonVariablesDto.getCustomerId(), commonVariablesDto.getClientId());
                responseDto = paybooksSdk.callAddBulkEmployees(accessToken, companyDomain, bulkAddEmployeeDto);
        }

        if (refreshCount == 0) {
            responseDto.setStatus(403L);
            responseDto.setMessage("Unauthorised access! RefreshCount exceeded");

            return responseDto;
        }

        if (responseDto.getStatus() == 200 ) {
            for (EmployeeDto employee: bulkAddEmployeeDto.getEmployees()) {
                PayrollUserInfo payrollUserInfo = new PayrollUserInfo();
                payrollUserInfo.setUserId(employee.getUserId());
                payrollUserInfo.setCustomerId(commonVariablesDto.getCustomerId());
                payrollUserInfo.setClientId(commonVariablesDto.getClientId());
                payrollUserInfo.setExternalUserId(employee.getCode());

                payrollUserInfoRepository.save(payrollUserInfo);
            }
        }
        return responseDto;
    }

    @Override
    public PutResponseDto updateEmployeeByCode(CommonVariablesDto commonVariablesDto, UpdateEmployeeDto updateEmployeeDto) throws JsonProcessingException, UnsupportedEncodingException {
        int refreshCount = 3;
        String accessToken = getToken(commonVariablesDto.getCustomerId(), commonVariablesDto.getClientId());
        String companyDomain = getCompanyDomain(commonVariablesDto.getCustomerId(), commonVariablesDto.getClientId());

        PutResponseDto responseDto = paybooksSdk.callUpdateEmployeeByCode(accessToken, companyDomain, updateEmployeeDto);

        while(responseDto.getStatus() == 401 && responseDto.getMessage().equalsIgnoreCase("Unauthorised access") && refreshCount>0) {
            refreshCount--;
            accessToken = refreshToken(commonVariablesDto.getCustomerId(), commonVariablesDto.getClientId());
            responseDto = paybooksSdk.callUpdateEmployeeByCode(accessToken, companyDomain, updateEmployeeDto);
        }

        if (refreshCount == 0) {
            responseDto.setStatus(403L);
            responseDto.setMessage("Unauthorised access! RefreshCount exceeded");

            return responseDto;
        }
        return responseDto;
    }

    @Override
    public PutResponseDto employeeResignation(CommonVariablesDto commonVariablesDto, EmployeeResignationDto employeeResignationDto) throws JsonProcessingException, UnsupportedEncodingException {
        int refreshCount = 3;
        String accessToken = getToken(commonVariablesDto.getCustomerId(), commonVariablesDto.getClientId());
        String companyDomain = getCompanyDomain(commonVariablesDto.getCustomerId(), commonVariablesDto.getClientId());

        PutResponseDto responseDto = paybooksSdk.callEmployeeResignation(accessToken, companyDomain, employeeResignationDto);

        while(responseDto.getStatus() == 401 && responseDto.getMessage().equalsIgnoreCase("Unauthorised access") && refreshCount>0) {
            refreshCount--;
            accessToken = refreshToken(commonVariablesDto.getCustomerId(), commonVariablesDto.getClientId());
            responseDto = paybooksSdk.callEmployeeResignation(accessToken, companyDomain, employeeResignationDto);
        }

        if (refreshCount == 0) {
            responseDto.setStatus(403L);
            responseDto.setMessage("Unauthorised access! RefreshCount exceeded");

            return responseDto;
        }
        return responseDto;
    }

    @Override
    public BulkResponseDto employeeSalaryUpdate(CommonVariablesDto commonVariablesDto, SalaryDto salaryDto) throws JsonProcessingException, UnsupportedEncodingException {
        int refreshCount = 3;
        String accessToken = getToken(commonVariablesDto.getCustomerId(), commonVariablesDto.getClientId());
        String companyDomain = getCompanyDomain(commonVariablesDto.getCustomerId(), commonVariablesDto.getClientId());

        BulkResponseDto responseDto = paybooksSdk.callEmployeeSalaryUpdate(accessToken, companyDomain, salaryDto);

        while(responseDto.getStatus() == 401 && responseDto.getMessage().equalsIgnoreCase("Unauthorised access") && refreshCount>0) {
            refreshCount--;
            accessToken = refreshToken(commonVariablesDto.getCustomerId(), commonVariablesDto.getClientId());
            responseDto = paybooksSdk.callEmployeeSalaryUpdate(accessToken, companyDomain, salaryDto);
        }

        if (refreshCount == 0) {
            responseDto.setStatus(403L);
            responseDto.setMessage("Unauthorised access! RefreshCount exceeded");

            return responseDto;
        }

        return responseDto;

    }

    public String getToken(Long customerId, Long clientId) throws UnsupportedEncodingException {
        String accessToken = null;

        Optional<PaybooksCompanyInfo> paybooksCompanyInfo = paybooksCompanyInfoRepository.findById(new PaybooksCompanyInfoId(customerId, clientId));

        if (paybooksCompanyInfo.isPresent()) {
            if (paybooksCompanyInfo.get().getAccessToken() != null) {
                accessToken = paybooksCompanyInfo.get().getAccessToken();
            } else {
                accessToken = generateAccessToken(paybooksCompanyInfo);
            }
        }

        return accessToken;
    }

    public String refreshToken(Long customerId, Long clientId) throws UnsupportedEncodingException {

        Optional<PaybooksCompanyInfo> paybooksCompanyInfo = paybooksCompanyInfoRepository.findById(new PaybooksCompanyInfoId(customerId, clientId));

        return generateAccessToken(paybooksCompanyInfo);

    }

    private String generateAccessToken(Optional<PaybooksCompanyInfo> paybooksCompanyInfo) throws UnsupportedEncodingException {
        String companyHash = paybooksCompanyInfo.get().getCompanyHash();
        String companyDomain = paybooksCompanyInfo.get().getCompanyDomain();

        String accessToken = tokenService.createJWT(companyDomain, companyHash);

        paybooksCompanyInfo.get().setAccessToken(accessToken);
        PaybooksCompanyInfo savedPaybooksCompanyInfo = paybooksCompanyInfoRepository.save(paybooksCompanyInfo.get());

        return accessToken;

    }

    private String getCompanyDomain(Long customerId, Long clientId) {
        Optional<PaybooksCompanyInfo> paybooksCompanyInfo = paybooksCompanyInfoRepository.findById(new PaybooksCompanyInfoId(customerId, clientId));

        if (paybooksCompanyInfo.isPresent()) {
            return paybooksCompanyInfo.get().getCompanyDomain();
        } else {
            return "Company domain does not exist";
        }
    }
}
