package com.peoplehum.payrollservice.common.util;

import com.peoplehum.payrollservice.common.entity.ClientInfo;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.function.Function;

@Service
public class JwtUtil {

    private String secret = "password";

    public String extractUsername(String token) throws UnsupportedEncodingException {
        return extractClaim(token, Claims::getSubject);
    }

    public Date extractExpiration(String token) throws UnsupportedEncodingException {
        return extractClaim(token, Claims::getExpiration);
    }

    public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) throws UnsupportedEncodingException {
        final Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }
    private Claims extractAllClaims(String token) throws UnsupportedEncodingException {
        return Jwts.parser().setSigningKey(secret.getBytes("UTF-8")).parseClaimsJws(token).getBody();
    }


    private Boolean isTokenExpired(String token) throws UnsupportedEncodingException {
        return extractExpiration(token).before(new Date());
    }

    public Boolean validateToken(String token, ClientInfo clientInfo) throws UnsupportedEncodingException {
        final String username = extractUsername(token);

        return(username.equals(clientInfo.getClientName()) && !isTokenExpired(token));
//        return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
    }
}
